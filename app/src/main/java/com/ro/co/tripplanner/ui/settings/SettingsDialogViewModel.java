package com.ro.co.tripplanner.ui.settings;


import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;

public class SettingsDialogViewModel {

    @NonNull
    public final ObservableField<String> appVersion = new ObservableField<>("");

}
