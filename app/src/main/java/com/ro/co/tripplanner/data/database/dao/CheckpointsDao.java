package com.ro.co.tripplanner.data.database.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.ro.co.tripplanner.data.database.models.Checkpoint;

import java.util.List;

import io.reactivex.Maybe;
import io.reactivex.Single;

@Dao
public interface CheckpointsDao {

  @Insert(onConflict = OnConflictStrategy.REPLACE)
  long[] insert(List<Checkpoint> checkpoints);

  @Query("UPDATE Checkpoint SET distanceToNextCheckpoint = :distanceToNext , durationToNextCheckpoint = :durationToNextCheckpoint  WHERE checkpointId = :checkpointId")
  void updateCheckpointById(int checkpointId, Integer distanceToNext, Integer durationToNextCheckpoint);

  @Query("DELETE FROM Checkpoint")
  int deleteAll();

  @Query("SELECT * FROM Checkpoint WHERE tourId= :tourId")
  Single<List<Checkpoint>> getAllCheckpointsForTripId(String tourId);

  @Query("SELECT * FROM Checkpoint WHERE checkpointId >= :originCheckpointId AND checkpointId BETWEEN :startCheckPointId AND :endCheckpointId LIMIT :maximumCheckpointsToRetrieve")
  Single<List<Checkpoint>> getNextCheckpointsFromOrigin(int originCheckpointId, int maximumCheckpointsToRetrieve, int startCheckPointId, int endCheckpointId);

  @Query("SELECT DISTINCT SUM(distanceToNextCheckpoint) FROM Checkpoint")
  Maybe<Integer> getDistanceForEntireTour();

  @Query("SELECT DISTINCT SUM(durationToNextCheckpoint) FROM Checkpoint")
  Maybe<Integer> getDrivingTimeForEntireTour();

  @Query("SELECT SUM(distanceToNextCheckpoint) FROM Checkpoint WHERE checkpointId BETWEEN :startCheckpointId AND :endCheckPointId")
  Maybe<Integer> getDistanceBetweenTwoCheckpoints(int startCheckpointId, int endCheckPointId);

  @Query("SELECT SUM(durationToNextCheckpoint) FROM Checkpoint WHERE checkpointId BETWEEN :startCheckpointId AND :endCheckPointId")
  Maybe<Integer> getDrivingTimeBetweenTwoCheckpoints(int startCheckpointId, int endCheckPointId);

  @Query("SELECT * FROM Checkpoint WHERE checkpointId BETWEEN :startCheckpointId AND :endCheckPointId")
  Maybe<List<Checkpoint>> getAllCheckpointsBetweenStartAndEndCheckpointIds(int startCheckpointId, int endCheckPointId);
}
