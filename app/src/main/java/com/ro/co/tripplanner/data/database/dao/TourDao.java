package com.ro.co.tripplanner.data.database.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.ro.co.tripplanner.data.database.models.Tour;

import java.util.List;

import io.reactivex.Maybe;
import io.reactivex.Single;

@Dao
public interface TourDao {

  @Query("SELECT tourId FROM TOUR WHERE isCurrentSelection= 1")
  Single<String> getCurrentlySelectedTripId();

  @Query("SELECT name FROM TOUR WHERE isCurrentSelection= 1")
  Maybe<String> getCurrentlySelectedTourName();

  @Query("UPDATE TOUR SET isCurrentSelection = :isSelected WHERE tourId = :tourId")
  void updateTourSelectionById(String tourId, Integer isSelected);

  @Insert(onConflict = OnConflictStrategy.REPLACE)
  long[] insert(List<Tour> toursList);

  @Query("DELETE FROM TOUR")
  int deleteAll();
}
