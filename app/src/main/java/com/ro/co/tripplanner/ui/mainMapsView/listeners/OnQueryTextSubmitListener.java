package com.ro.co.tripplanner.ui.mainMapsView.listeners;

public interface OnQueryTextSubmitListener {

  boolean onQueryTextSubmit(String query);
}
