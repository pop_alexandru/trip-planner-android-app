package com.ro.co.tripplanner.ui.mainMapsView.chartView;

import androidx.annotation.Nullable;

public class ElevationPoint {

  private int orderInTripId;
  @Nullable
  private Float altitude;

  public ElevationPoint(int orderInTripId, @Nullable Float altitude) {
    this.orderInTripId = orderInTripId;
    this.altitude = altitude;
  }

  int getOrderInTripId() {
    return orderInTripId;
  }

  @Nullable
  Float getAltitude() {
    return altitude;
  }
}
