package com.ro.co.tripplanner.ui.settings;

import androidx.annotation.NonNull;

import com.ro.co.tripplanner.app.Injection;
import com.ro.co.tripplanner.ui.base.BasePresenter;


public class SettingsDialogPresenter extends BasePresenter implements SettingsDialogContract.Presenter {

    @NonNull
    private static final String USER_TOKEN = "user_token";

    @NonNull
    private final SettingsDialogContract.View view;

    SettingsDialogPresenter(@NonNull SettingsDialogContract.View view) {
        this.view = view;
    }

    @Override
    public void onDismissButtonClicked() {
        if (isViewAttached) {
            view.dismissDialog();
        }
    }

    @Override
    public void onSignOutButtonClicked() {
        boolean isTokenRemoved = Injection.provideSharedPreferences()
            .edit()
            .remove(SettingsDialogPresenter.USER_TOKEN)
            .commit();
        if (isTokenRemoved && isViewAttached) {
            view.switchToLoginScreen();
        }
    }
}
