package com.ro.co.tripplanner.domain.models;

import android.util.Pair;

import androidx.annotation.Nullable;

import com.ro.co.tripplanner.data.database.models.RouteLeg;
import com.ro.co.tripplanner.data.database.models.RouteStep;
import com.ro.co.tripplanner.ui.mainMapsView.models.MapCheckpoint;

import java.util.List;

public class SelectedTripMapData {

  @Nullable
  private List<MapCheckpoint> tripCheckpoints;
  @Nullable
  private List<Pair<RouteLeg, List<RouteStep>>> tripPolylineData;
  @Nullable
  private Integer tripDuration;
  @Nullable
  private Integer tripDistance;
  @Nullable
  private String tripTitle;

  public SelectedTripMapData(@Nullable List<MapCheckpoint> tripCheckpoints,
                             @Nullable List<Pair<RouteLeg, List<RouteStep>>> tripPolylineData,
                             @Nullable Integer tripDuration,
                             @Nullable Integer tripDistance,
                             @Nullable String tripTitle) {
    this.tripCheckpoints = tripCheckpoints;
    this.tripPolylineData = tripPolylineData;
    this.tripDuration = tripDuration;
    this.tripDistance = tripDistance;
    this.tripTitle = tripTitle;
  }

  @Nullable
  public List<MapCheckpoint> getTripCheckpoints() {
    return tripCheckpoints;
  }

  @Nullable
  public List<Pair<RouteLeg, List<RouteStep>>> getTripPolylineData() {
    return tripPolylineData;
  }

  @Nullable
  public Integer getTripDuration() {
    return tripDuration;
  }

  @Nullable
  public Integer getTripDistance() {
    return tripDistance;
  }

  @Nullable
  public String getTripTitle() {
    return tripTitle;
  }
}
