package com.ro.co.tripplanner.ui.mainMapsView.listeners;

public interface OnSearchViewCloseListener {

  boolean onSearchViewClosed(boolean isClosed);
}
