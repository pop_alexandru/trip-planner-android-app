package com.ro.co.tripplanner.ui.chooseTour;

import androidx.annotation.NonNull;

import com.ro.co.tripplanner.R;
import com.ro.co.tripplanner.app.Injection;
import com.ro.co.tripplanner.data.network.models.response.dailyTour.TripDetailsResponse;
import com.ro.co.tripplanner.domain.useCases.GetAllAvailableTripsUseCase;
import com.ro.co.tripplanner.ui.base.BasePresenter;
import com.ro.co.tripplanner.ui.mainMapsView.listeners.OnSelectedTourListener;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class ChooseTourDialogPresenter extends BasePresenter implements ChooseTourDialogContract.Presenter {

    @NonNull
    private final ChooseTourDialogContract.View view;
    @NonNull
    private final List<TripDetailsResponse> tripDetailsResponses = new ArrayList<>();

    ChooseTourDialogPresenter(@NonNull ChooseTourDialogContract.View view) {
        this.view = view;
    }

    @Override
    public void onAttachView() {
        super.onAttachView();
        retrieveAvailableToursFromRemoteSource();
    }

    @Override
    public void onDismissButtonClicked() {
        if (isViewAttached) {
            view.dismissDialog();
        }
    }

    @Override
    public void OnSelectionSaved(@NonNull OnSelectedTourListener callback, @NonNull String tourId) {
        callback.OnSelectedTour(tourId, tripDetailsResponses);
    }

    private void retrieveAvailableToursFromRemoteSource() {
        tripDetailsResponses.clear();
      addSubscription(new GetAllAvailableTripsUseCase(Schedulers.io(), AndroidSchedulers.mainThread(),
            Injection.provideBackendApi())
            .perform()
            .filter(tripDetailsResponse -> isViewAttached)
            .doOnSubscribe(disposable -> view.showLoadingView(true))
            .subscribe(this::onGetTripDetailsResponse, this::doOnGetTripDetailsError,
                this::doOnGetTripDetailsComplete));

    }

    private void onGetTripDetailsResponse(TripDetailsResponse response) {
        if (response != null) {
            tripDetailsResponses.add(response);
        }
    }

    private void doOnGetTripDetailsError(Throwable throwable) {
        throwable.printStackTrace();
        view.showMessage(getString(R.string.message_server_down));
        view.dismissDialog();
    }

    private void doOnGetTripDetailsComplete() {
        view.showLoadingView(false);
        convertResponseAndLoad();
    }

    private void convertResponseAndLoad() {
        List<TourModel> tourModels = new ArrayList<>();
        for (TripDetailsResponse response : tripDetailsResponses) {
            String position = String.valueOf(tripDetailsResponses.indexOf(response));
            tourModels.add(new TourModel(response.getId(), position, response.getName(), this));
        }
        view.loadAvailableTours(tourModels);
    }

    @Override
    public void OnTourClicked(String tourId) {
        view.saveSelectionAndDismiss(tourId);
    }
}
