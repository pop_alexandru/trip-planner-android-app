package com.ro.co.tripplanner.data.network.models.response.routes;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RouteResponse {

  @SerializedName("legs")
  @Expose
  private List<Leg> legs = null;
  @SerializedName("overview_polyline")
  @Expose
  private OverviewPolyline overviewPolyline;

  public List<Leg> getLegs() {
    return legs;
  }

  public OverviewPolyline getOverviewPolyline() {
    return overviewPolyline;
  }

}
