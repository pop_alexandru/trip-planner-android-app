package com.ro.co.tripplanner.domain.services;

class RequestDirectionsError extends Throwable {

  RequestDirectionsError(String message) {
    super(message);
  }
}
