package com.ro.co.tripplanner.domain.services;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import android.util.Pair;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.ro.co.tripplanner.R;
import com.ro.co.tripplanner.app.Injection;
import com.ro.co.tripplanner.data.database.LocalStorageManager;
import com.ro.co.tripplanner.data.database.models.Checkpoint;
import com.ro.co.tripplanner.data.database.models.Route;
import com.ro.co.tripplanner.data.database.models.RouteLeg;
import com.ro.co.tripplanner.data.database.models.RouteStep;
import com.ro.co.tripplanner.data.network.GoogleMapsAPI;
import com.ro.co.tripplanner.data.network.NetworkExceptions;
import com.ro.co.tripplanner.data.network.NetworkRequestBuilders;
import com.ro.co.tripplanner.data.network.NetworkResponseConverter;
import com.ro.co.tripplanner.data.network.models.request.RouteDirectionsRequest;
import com.ro.co.tripplanner.data.network.models.response.routes.Leg;
import com.ro.co.tripplanner.data.network.models.response.routes.RouteResponse;
import com.ro.co.tripplanner.data.network.models.response.routes.RoutesResponse;
import com.ro.co.tripplanner.data.network.models.response.routes.Step;
import com.ro.co.tripplanner.ui.utils.ArrayUtils;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Maybe;
import io.reactivex.MaybeSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import okhttp3.internal.http2.StreamResetException;

public class RequestDirectionsService extends Service {

  @NonNull
  private static final String TAG = RequestDirectionsService.class.getSimpleName();

  private final static int NUMBER_OF_MAX_CHECKPOINTS_PER_DIRECTIONS_REQUEST = 12;

  @NonNull
  private final IBinder localServiceBinder = new RequestDirectionsService.RouteDirectionsLocalBinder();
  @NonNull
  private final LocalStorageManager storageManager = Injection.provideStorageManager();
  @NonNull
  private final GoogleMapsAPI googleMapsAPI = Injection.provideDirectionsApi();
  @NonNull
  private final String googleApiKey = Injection.provideGlobalContext()
      .getResources()
      .getString(R.string.google_maps_key);

  @Nullable
  private Disposable directionsRequestsDisposable;

  @Override
  public IBinder onBind(Intent intent) {
    return localServiceBinder;
  }

  @Override
  public boolean onUnbind(Intent intent) {
    return true;
  }

  @Override
  public int onStartCommand(Intent intent, int flags, int startId) {
    loadCheckpointsForSelectedTrip();
    return Service.START_NOT_STICKY;
  }

  @Override
  public void onDestroy() {
    if (directionsRequestsDisposable != null) {
      directionsRequestsDisposable.dispose();
    }
  }

  private void loadCheckpointsForSelectedTrip() {
    directionsRequestsDisposable = storageManager.tourDao()
        .getCurrentlySelectedTripId()
        .flatMapMaybe(getAllCheckpointsForTripId())
        .subscribeOn(Schedulers.io())
        .observeOn(Schedulers.io())
        .subscribe(this::onLoadTripCheckpointsSuccess, this::onLoadTripCheckpointsError);
  }

  private Function<String, MaybeSource<Pair<String, List<Checkpoint>>>> getAllCheckpointsForTripId() {
    return tripId -> storageManager.checkpointsDao()
        .getAllCheckpointsForTripId(tripId)
        .flatMapMaybe((Function<List<Checkpoint>, MaybeSource<Pair<String, List<Checkpoint>>>>)
            checkpoints -> Maybe.just(new Pair<>(tripId, checkpoints)));
  }

  private void onLoadTripCheckpointsSuccess(@NonNull Pair<String, List<Checkpoint>> tripAndCheckpoints) {

    List<Maybe<Boolean>> routeDirectionsApiCallList = new ArrayList<>();

    List<List<Checkpoint>> totalBatchedRouteRequests =
        ArrayUtils.splitCheckpointsIntoBatches(tripAndCheckpoints.second,
            RequestDirectionsService.NUMBER_OF_MAX_CHECKPOINTS_PER_DIRECTIONS_REQUEST);

    for (int i = 0; i < totalBatchedRouteRequests.size(); i++) {
      List<Checkpoint> routeRequestCheckpointList = totalBatchedRouteRequests.get(i);
      RouteDirectionsRequest routeDirectionsRequest = NetworkRequestBuilders
          .generateDirectionRequestParameters(routeRequestCheckpointList, googleApiKey);

      Maybe<Boolean> routeDirectionsApiCall =
          getDirectionsForRoute(routeDirectionsRequest)
              .flatMap(mapRouteDirectionsToLocalStorage(tripAndCheckpoints.first,
                  routeRequestCheckpointList));

      routeDirectionsApiCallList.add(routeDirectionsApiCall);
    }
    startRouteDirectionsRequests(routeDirectionsApiCallList);
  }

  private void onLoadTripCheckpointsError(@NonNull Throwable throwable) {
    Log.e(TAG, "Got error on loading trip checkpoints " + throwable.getMessage());
    stopSelf();
  }

  private void startRouteDirectionsRequests(@NonNull List<Maybe<Boolean>> routeDirectionsApiCallList) {
    directionsRequestsDisposable = Maybe.concat(routeDirectionsApiCallList)
        .doOnSubscribe(subscription -> onStartGetDirectionsRequest())
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(aBoolean -> {
          // Do nothing
        }, this::onGetRouteDirectionsError, this::onGetRouteDirectionsSuccess);
  }

  private void onStartGetDirectionsRequest() {
    ServiceStatusBroadcastManager.getInstance()
        .broadcastDirectionRequestProgress(true);
  }

  private void onGetRouteDirectionsError(@NonNull Throwable throwable) {
    Log.e(TAG, "Got error on getting route directions " + throwable.getMessage());
    ServiceStatusBroadcastManager broadcastManager = ServiceStatusBroadcastManager.getInstance();
    if (throwable instanceof UnknownHostException) {
      broadcastManager.broadcastRequestError(NetworkExceptions.UNKNOWN_HOST);
    } else if (throwable instanceof StreamResetException) {
      broadcastManager.broadcastRequestError(NetworkExceptions.STREAM_RESET_EXCEPTION);
    } else if (throwable instanceof RequestDirectionsError) {
      broadcastManager.broadcastRequestError(NetworkExceptions.TRIP_DIRECTIONS_COULD_NOT_BE_OBTAINED_ERROR);
    } else {
      broadcastManager.broadcastRequestError(NetworkExceptions.GENERIC_ERROR);
    }
    stopSelf();
  }

  private void onGetRouteDirectionsSuccess() {
    ServiceStatusBroadcastManager.getInstance()
        .broadcastDirectionRequestProgress(false);
    stopSelf();
  }

  @NonNull
  public Maybe<RoutesResponse> getDirectionsForRoute(@NonNull RouteDirectionsRequest routeDirectionsRequest) {
    return googleMapsAPI.getDirectionsForCheckpoints(routeDirectionsRequest.startWaypoint, routeDirectionsRequest.endWaypoint,
        routeDirectionsRequest.transitWaypoints, routeDirectionsRequest.apiKey);
  }

  @NonNull
  private Function<RoutesResponse, MaybeSource<Boolean>> mapRouteDirectionsToLocalStorage(
      @NonNull String tripId,
      @NonNull List<Checkpoint> routeRequestCheckpoints) {
    return routesResponse -> {
      if (routesResponse == null || routesResponse.getRoutes() == null) {
        return Maybe.error(new Throwable());
      }
      if (routesResponse.getRoutes().isEmpty()) {
        return Maybe.error(new RequestDirectionsError("Directions API returned an " +
            "empty response set"));
      }

      RouteResponse routeResponse = routesResponse.getRoutes().get(0);
      return Maybe.fromCallable(() -> {
        String routePolyline = routeResponse.getOverviewPolyline().getPoints();

        Route route = NetworkResponseConverter.convertToRouteDatabaseModel(routePolyline, tripId);
        long routeId = storageManager.routeDao()
            .insert(route);

        List<Leg> responseLegs = routeResponse.getLegs();

        for (int i = 0; i < responseLegs.size(); i++) {
          Leg legResponse = responseLegs.get(i);

          int routeStartCheckpointId = routeRequestCheckpoints.get(i)
              .getCheckpointId();
          int routeEndCheckpointId = routeRequestCheckpoints.get(i + 1)
              .getCheckpointId();
          int checkpointId = routeRequestCheckpoints.get(i)
              .getCheckpointId();

          updateRouteLegsAndStepsInLocalStorage(legResponse, (int) routeId,
              routeStartCheckpointId, routeEndCheckpointId);

          updateDistanceAndDurationInLocalStorage(legResponse, checkpointId);
        }
        return true;
      });
    };
  }

  /**
   * Saves the legs response and associated steps
   */
  private void updateRouteLegsAndStepsInLocalStorage(@NonNull Leg legResponse,
                                                     int routeId,
                                                     int routeStartCheckpointId,
                                                     int routeEndCheckpointId) {
    RouteLeg routeLeg = NetworkResponseConverter.convertResponseToRouteLeg(legResponse, routeId,
        routeStartCheckpointId, routeEndCheckpointId);

    long routeLegId = storageManager.routeLegDao()
        .insertRouteLeg(routeLeg);

    List<Step> responseSteps = legResponse.getSteps();
    List<RouteStep> routeStepsList = new ArrayList<>();

    for (Step stepResponse : responseSteps) {
      RouteStep routeStep = NetworkResponseConverter.convertResponseToRouteStep(stepResponse, (int) routeLegId);
      routeStepsList.add(routeStep);
    }
    storageManager.routeStepDao()
        .insertRouteLeg(routeStepsList);
  }

  private void updateDistanceAndDurationInLocalStorage(@NonNull Leg responseLeg, int checkpointId) {
    int distanceToNext = responseLeg
        .getDistance()
        .getValue();
    int durationToNext = responseLeg
        .getDuration()
        .getValue();
    storageManager.checkpointsDao()
        .updateCheckpointById(checkpointId, distanceToNext, durationToNext);
  }

  private class RouteDirectionsLocalBinder extends Binder {
    // Do nothing
  }

}


