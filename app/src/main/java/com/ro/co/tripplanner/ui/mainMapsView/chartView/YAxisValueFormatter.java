package com.ro.co.tripplanner.ui.mainMapsView.chartView;

import androidx.annotation.NonNull;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.text.DecimalFormat;

public class YAxisValueFormatter implements IAxisValueFormatter {

    private final static float KILOMETER_VALUE = 1000;
    @NonNull
    private final DecimalFormat mFormat = new DecimalFormat("###,###,###");

    @Override
    public String getFormattedValue(float value, AxisBase axis) {
        if (value >= YAxisValueFormatter.KILOMETER_VALUE) {
            value = value / YAxisValueFormatter.KILOMETER_VALUE;
            return mFormat.format(value) + " Km";
        } else {
            return mFormat.format(value) + " m";
        }
    }
}
