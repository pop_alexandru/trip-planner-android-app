package com.ro.co.tripplanner.data.network.models.response.routes;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LegEndLocation {

  @SerializedName("lat")
  @Expose
  private Double lat;
  @SerializedName("lng")
  @Expose
  private Double lng;

  public Double getLat() {
    return lat;
  }

  public Double getLng() {
    return lng;
  }

}
