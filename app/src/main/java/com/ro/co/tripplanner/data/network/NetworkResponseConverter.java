package com.ro.co.tripplanner.data.network;

import androidx.annotation.NonNull;

import com.ro.co.tripplanner.data.database.models.Checkpoint;
import com.ro.co.tripplanner.data.database.models.Route;
import com.ro.co.tripplanner.data.database.models.RouteLeg;
import com.ro.co.tripplanner.data.database.models.RouteStep;
import com.ro.co.tripplanner.data.network.models.response.dailyTour.TripCheckpoint;
import com.ro.co.tripplanner.data.network.models.response.routes.Leg;
import com.ro.co.tripplanner.data.network.models.response.routes.Step;

import java.util.ArrayList;
import java.util.List;

public final class NetworkResponseConverter {

  private NetworkResponseConverter() {
  }

  @NonNull
  public static Route convertToRouteDatabaseModel(@NonNull String routePolyline, @NonNull String tourId) {
    Route route = new Route();
    route.setTourId(tourId);
    route.setRoutePolyline(routePolyline);
    return route;
  }

  @NonNull
  public static List<Checkpoint> convertResponseToCheckpoints(@NonNull Iterable<TripCheckpoint> tourResponseCheckpoints, @NonNull String tourId)
      throws NullPointerException {
    List<Checkpoint> toSaveCheckpoints = new ArrayList<>();
    for (TripCheckpoint tripCheckpoint : tourResponseCheckpoints) {
      Checkpoint checkpoint = new Checkpoint();
      checkpoint.setOrderInTourId(tripCheckpoint.getOrderInTrip());
      checkpoint.setCheckpointName(tripCheckpoint.getName());
      checkpoint.setTourId(tourId);
      checkpoint.setLatitude(tripCheckpoint.getLatitude());
      checkpoint.setLongitude(tripCheckpoint.getLongitude());
      checkpoint.setAltitude(tripCheckpoint.getAltitude());
      toSaveCheckpoints.add(checkpoint);
    }
    return toSaveCheckpoints;
  }

  @NonNull
  public static RouteLeg convertResponseToRouteLeg(@NonNull Leg legResponse, int routeId, int routeStartCheckpointId, int routeEndCheckpointId) {
    RouteLeg routeLeg = new RouteLeg();
    routeLeg.setRouteId(routeId);
    routeLeg.setStartCheckpointId(routeStartCheckpointId);
    routeLeg.setEndCheckpointId(routeEndCheckpointId);

    routeLeg.setRouteLegStartLatitude(legResponse.getStartLocation()
        .getLat());
    routeLeg.setRouteLegStartLongitude(legResponse.getStartLocation()
        .getLng());
    routeLeg.setRouteLegEndLatitude(legResponse.getEndLocation()
        .getLat());
    routeLeg.setRouteLegEndLongitude(legResponse.getEndLocation()
        .getLng());
    return routeLeg;
  }

  @NonNull
  public static RouteStep convertResponseToRouteStep(@NonNull Step stepResponse, int routeLegId) {
    RouteStep routeStep = new RouteStep();
    routeStep.setRouteLegId(routeLegId);
    routeStep.setRouteStepStartLatitude(stepResponse.getStartLocation()
        .getLat());
    routeStep.setRouteStepStartLongitude(stepResponse.getStartLocation()
        .getLng());
    routeStep.setRouteStepEndLatitude(stepResponse.getEndLocation()
        .getLat());
    routeStep.setRouteStepEndLongitude(stepResponse.getEndLocation()
        .getLng());
    routeStep.setRouteStepPolyline(stepResponse.getPolyline()
        .getPoints());
    return routeStep;
  }
}
