package com.ro.co.tripplanner.data.database.models;


import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity()
public class Tour {

  @PrimaryKey
  @NonNull
  private String tourId;
  private String name;
  private boolean isEntireTour;
  private boolean isCurrentSelection;

  public String getTourId() {
    return tourId;
  }

  public void setTourId(String tourId) {
    this.tourId = tourId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public boolean isEntireTour() {
    return isEntireTour;
  }

  public void setEntireTour(boolean entireTour) {
    isEntireTour = entireTour;
  }

  public boolean isCurrentSelection() {
    return isCurrentSelection;
  }

  public void setCurrentSelection(boolean currentSelection) {
    isCurrentSelection = currentSelection;
  }

  @NonNull
  @Override
  public String toString() {
    return name;
  }
}
