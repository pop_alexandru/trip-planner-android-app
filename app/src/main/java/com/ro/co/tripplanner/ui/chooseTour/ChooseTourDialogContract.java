package com.ro.co.tripplanner.ui.chooseTour;

import androidx.annotation.NonNull;

import com.ro.co.tripplanner.ui.base.BaseContract;
import com.ro.co.tripplanner.ui.mainMapsView.listeners.OnSelectedTourListener;

import java.util.List;

public class ChooseTourDialogContract extends BaseContract {

  public interface View extends BaseContract.View {

    void loadAvailableTours(@NonNull List<TourModel> tours);

    void dismissDialog();

    void saveSelectionAndDismiss(@NonNull String tourId);
  }

  public interface Presenter extends BaseContract.Presenter, TourClickedListener {

    void onDismissButtonClicked();

    void OnSelectionSaved(@NonNull OnSelectedTourListener callback, @NonNull String tourId);
  }

}
