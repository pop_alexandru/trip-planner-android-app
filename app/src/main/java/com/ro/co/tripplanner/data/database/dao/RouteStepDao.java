package com.ro.co.tripplanner.data.database.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.ro.co.tripplanner.data.database.models.RouteStep;

import java.util.List;

@Dao
public interface RouteStepDao {

  @Insert(onConflict = OnConflictStrategy.REPLACE)
  long[] insertRouteLeg(List<RouteStep> routeSteps);

  @Query("DELETE FROM RouteStep")
  int deleteAll();

  @Query("SELECT * FROM ROUTESTEP WHERE routeLegId = :legId")
  List<RouteStep> getStepsForLegId(int legId);

}
