package com.ro.co.tripplanner.domain.base;

public interface UseCaseDefinition<T> {

  T perform();

}
