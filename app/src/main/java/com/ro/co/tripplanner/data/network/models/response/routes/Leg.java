package com.ro.co.tripplanner.data.network.models.response.routes;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Leg {

  @SerializedName("distance")
  @Expose
  private Distance distance;
  @SerializedName("duration")
  @Expose
  private Duration duration;
  @SerializedName("end_address")
  @Expose
  private String endAddress;
  @SerializedName("end_location")
  @Expose
  private LegEndLocation endLocation;
  @SerializedName("start_location")
  @Expose
  private LegStartLocation startLocation;
  @SerializedName("steps")
  @Expose
  private List<Step> steps = null;

  public Distance getDistance() {
    return distance;
  }

  public Duration getDuration() {
    return duration;
  }

  public LegEndLocation getEndLocation() {
    return endLocation;
  }

  public LegStartLocation getStartLocation() {
    return startLocation;
  }

  public List<Step> getSteps() {
    return steps;
  }

}
