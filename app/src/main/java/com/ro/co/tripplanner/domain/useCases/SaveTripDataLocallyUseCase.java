package com.ro.co.tripplanner.domain.useCases;

import androidx.annotation.NonNull;

import com.ro.co.tripplanner.data.database.LocalStorageManager;
import com.ro.co.tripplanner.data.database.models.Checkpoint;
import com.ro.co.tripplanner.data.database.models.Tour;
import com.ro.co.tripplanner.data.network.NetworkResponseConverter;
import com.ro.co.tripplanner.data.network.models.response.dailyTour.TripCheckpoint;
import com.ro.co.tripplanner.data.network.models.response.dailyTour.TripDetailsResponse;
import com.ro.co.tripplanner.domain.base.BaseUseCase;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Scheduler;

public class SaveTripDataLocallyUseCase extends BaseUseCase {

  @NonNull
  private final LocalStorageManager localStorageManager;
  @NonNull
  private final List<TripDetailsResponse> tourDataList;
  @NonNull
  private final String selectedTourId;

  public SaveTripDataLocallyUseCase(@NonNull Scheduler executorThread,
                                    @NonNull Scheduler postExecutionThread,
                                    @NonNull LocalStorageManager localStorageManager,
                                    @NonNull List<TripDetailsResponse> tourDataList,
                                    @NonNull String selectedTourId) {
    super(executorThread, postExecutionThread);
    this.localStorageManager = localStorageManager;
    this.tourDataList = tourDataList;
    this.selectedTourId = selectedTourId;
  }

  @Override
  public Completable perform() {
    return Completable.fromAction(() -> {
      purgeEntireDatabase();

      List<Tour> tours = new ArrayList<>();
      for (TripDetailsResponse response : tourDataList) {
        Tour tour = new Tour();
        tour.setTourId(response.getId());
        tour.setName(response.getName());
        tours.add(tour);
      }
      localStorageManager.tourDao().insert(tours);
      // Setup the selected trio status
      localStorageManager.tourDao()
          .updateTourSelectionById(selectedTourId, 1);


      for (TripDetailsResponse response : tourDataList) {
        String tourId = response.getId();
        List<TripCheckpoint> tourResponseCheckpoints = response.getTripCheckpoints();
        try {
          List<Checkpoint> toSaveCheckpoints = NetworkResponseConverter
              .convertResponseToCheckpoints(tourResponseCheckpoints, tourId);
          localStorageManager.checkpointsDao().insert(toSaveCheckpoints);
        } catch (NullPointerException e) {
          e.printStackTrace();
        }
      }
    });
  }

  /**
   * Deletes entire database
   */
  private void purgeEntireDatabase() {
    localStorageManager.tourDao()
        .deleteAll();
    localStorageManager.routeDao()
        .deleteAll();
    localStorageManager.checkpointsDao()
        .deleteAll();
    localStorageManager.routeLegDao()
        .deleteAll();
    localStorageManager.routeStepDao()
        .deleteAll();
  }
}

