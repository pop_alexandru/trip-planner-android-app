package com.ro.co.tripplanner.data.network.models.response.routes;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Step {

  @SerializedName("end_location")
  @Expose
  private StepEndLocation endLocation;
  @SerializedName("polyline")
  @Expose
  private Polyline polyline;
  @SerializedName("start_location")
  @Expose
  private StepStartLocation startLocation;

  public StepEndLocation getEndLocation() {
    return endLocation;
  }

  public Polyline getPolyline() {
    return polyline;
  }

  public StepStartLocation getStartLocation() {
    return startLocation;
  }
}
