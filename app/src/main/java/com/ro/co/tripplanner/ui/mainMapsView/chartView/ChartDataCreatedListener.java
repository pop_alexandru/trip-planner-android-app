package com.ro.co.tripplanner.ui.mainMapsView.chartView;

import androidx.annotation.NonNull;

import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.data.LineData;


public interface ChartDataCreatedListener {

  void OnChartDataCreated(@NonNull LineData lineData, @NonNull Description description);

}
