package com.ro.co.tripplanner.domain.useCases;

import androidx.annotation.NonNull;

import com.ro.co.tripplanner.data.network.BackendAPI;
import com.ro.co.tripplanner.data.network.models.response.dailyTour.TripDetailsResponse;
import com.ro.co.tripplanner.data.network.models.response.dailyTour.TripResponse;
import com.ro.co.tripplanner.domain.base.BaseUseCase;
import com.ro.co.tripplanner.domain.base.UseCaseDefinition;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.functions.Function;

public class GetAllAvailableTripsUseCase extends BaseUseCase implements UseCaseDefinition {

  @NonNull
  private final BackendAPI backendAPI;

  public GetAllAvailableTripsUseCase(@NonNull Scheduler executorThread,
                                     @NonNull Scheduler postExecutionThread,
                                     @NonNull BackendAPI backendAPI) {
    super(executorThread, postExecutionThread);
    this.backendAPI = backendAPI;
  }

  @Override
  public Observable<TripDetailsResponse> perform() {
    return backendAPI.getAllTours()
        .switchMap(mapIndividualTripDetailsRequests())
        .subscribeOn(executorThread)
        .observeOn(postExecutionThread);
  }

  private Function<List<TripResponse>, Observable<TripDetailsResponse>> mapIndividualTripDetailsRequests() {
    return tripResponseList -> {
      List<Observable<TripDetailsResponse>> tripRequests = new ArrayList<>();

      if (tripResponseList != null) {
        for (TripResponse toursResponse : tripResponseList) {
          Observable<TripDetailsResponse> tourRequest =
              backendAPI.getTourById(toursResponse.getId());
          tripRequests.add(tourRequest);
        }
      }
      return Observable.concat(tripRequests);
    };
  }
}
