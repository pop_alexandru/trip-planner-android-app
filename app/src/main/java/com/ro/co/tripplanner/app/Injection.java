package com.ro.co.tripplanner.app;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;

import androidx.annotation.NonNull;

import com.ro.co.tripplanner.BuildConfig;
import com.ro.co.tripplanner.data.database.LocalStorageManager;
import com.ro.co.tripplanner.data.network.BackendAPI;
import com.ro.co.tripplanner.data.network.GoogleMapsAPI;
import com.ro.co.tripplanner.data.network.NetworkManager;


public class Injection {

  @NonNull
  private static final String SHARED_PREFERENCES = "app_status";
  @NonNull
  private static final String directionsBaseUrl = "https://maps.googleapis.com/maps/api/";
  @NonNull
  private static final String baseBackendUrl = BuildConfig.CLOUD_BACKEND_URL;

  /**
   * Provides the global Application Context, which is already a singleton, not an activity / fragment Context instance, not a memory leak .
   */
  @SuppressLint("StaticFieldLeak")
  @NonNull
  private static Context context;

  public static void initialize(@NonNull Context context) {
    Injection.context = context;
  }

  @NonNull
  public static Context provideGlobalContext() {
    return Injection.context;
  }

  @NonNull
  public static LocalStorageManager provideStorageManager() {
    return LocalStorageManager.getInstance(Injection.context);
  }

  @NonNull
  public static GoogleMapsAPI provideDirectionsApi() {
    return NetworkManager.getDirectionsAPIService(Injection.directionsBaseUrl);
  }

  @NonNull
  public static BackendAPI provideBackendApi() {
    return NetworkManager.getBackendAPIService(Injection.baseBackendUrl);
  }

  @NonNull
  public static SharedPreferences provideSharedPreferences() {
    return Injection.context.getSharedPreferences(Injection.SHARED_PREFERENCES, Context.MODE_PRIVATE);
  }

  @NonNull
  public static Resources provideResources() {
    return Injection.context.getResources();
  }

}
