package com.ro.co.tripplanner.ui.notifications;

import android.app.Notification;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationManagerCompat;

import com.ro.co.tripplanner.R;
import com.ro.co.tripplanner.app.Injection;


public class NotificationManager {

    private static NotificationManager instance;

    @NonNull
    public static NotificationManager getInstance() {
        if (NotificationManager.instance == null) {
            NotificationManager.instance = new NotificationManager();
            return NotificationManager.instance;
        }
        return NotificationManager.instance;
    }

    public void notifyAboutRouteDeviation() {
        Context ctx = Injection.provideGlobalContext();
        Notification notification = NotificationsUtils.createHighPriorityNotification(ctx, ctx.getString(R.string.message_notify_deviation),
            ctx.getString(R.string.title_warning));
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(ctx);
        notificationManager.notify(1200, notification);
    }
}
