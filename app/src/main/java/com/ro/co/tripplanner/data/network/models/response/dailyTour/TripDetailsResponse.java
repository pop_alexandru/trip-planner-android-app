package com.ro.co.tripplanner.data.network.models.response.dailyTour;

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TripDetailsResponse {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("tripCheckpoints")
    @Expose
    private List<TripCheckpoint> tripCheckpoints;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public List<TripCheckpoint> getTripCheckpoints() {
        return tripCheckpoints;
    }

    public void setTripCheckpoints(List<TripCheckpoint> tripCheckpoints) {
        this.tripCheckpoints = tripCheckpoints;
    }

    @NonNull
    @Override
    public String toString() {
        return name;
    }

}
