package com.ro.co.tripplanner.data.network;

import com.ro.co.tripplanner.data.network.models.response.routes.RoutesResponse;

import io.reactivex.Maybe;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface GoogleMapsAPI {

    @GET("directions/json")
    Maybe<RoutesResponse> getDirectionsForCheckpoints(@Query("origin") String origin, @Query("destination") String destination, @Query("waypoints") String checkpoints, @Query("key") String apiKey);


}
