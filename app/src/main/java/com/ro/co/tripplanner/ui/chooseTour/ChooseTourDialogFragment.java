package com.ro.co.tripplanner.ui.chooseTour;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;

import com.ro.co.tripplanner.R;
import com.ro.co.tripplanner.databinding.FragmentDialogRoutesBinding;
import com.ro.co.tripplanner.ui.base.BaseDialogFragment;
import com.ro.co.tripplanner.ui.mainMapsView.listeners.OnSelectedTourListener;

import java.util.List;

public class ChooseTourDialogFragment extends BaseDialogFragment<ChooseTourDialogContract.Presenter> implements ChooseTourDialogContract.View {

  @NonNull
  public static final String TAG = ChooseTourDialogFragment.class.getSimpleName();
  @NonNull
  private final ChooseTourDialogViewModel viewModel = new ChooseTourDialogViewModel();

  @NonNull
  public static ChooseTourDialogFragment createInstance() {
    return new ChooseTourDialogFragment();
  }

  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    FragmentDialogRoutesBinding binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.fragment_dialog_routes, null, false);
    binding.setViewModel(viewModel);
    binding.setPresenter(getPresenter());
    setupTransparentDialogBackground();
    return binding.getRoot();
  }

  @NonNull
  @Override
  public Dialog onCreateDialog(Bundle savedInstanceState) {
    return new Dialog(getActivity(), R.style.WideDialogStyle);
  }

  @Override
  public ChooseTourDialogContract.Presenter createPresenter() {
    return new ChooseTourDialogPresenter(this);
  }

  @Override
  public void showLoadingView(boolean isLoading) {
    viewModel.isLoadingInProgress.set(isLoading);
  }

  @Override
  public void loadAvailableTours(@NonNull List<TourModel> tours) {
    viewModel.availableTours.clear();
    viewModel.availableTours.addAll(tours);
  }

  @Override
  public void dismissDialog() {
    dismiss();
  }

  @Override
  public void saveSelectionAndDismiss(@NonNull String tourId) {
    OnSelectedTourListener callback = (OnSelectedTourListener) getParentFragment();
    if (callback != null) {
      getPresenter().OnSelectionSaved(callback, tourId);
    }
    dismiss();
  }
}
