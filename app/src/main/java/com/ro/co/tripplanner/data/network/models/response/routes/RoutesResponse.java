package com.ro.co.tripplanner.data.network.models.response.routes;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RoutesResponse {

  @SerializedName("routes")
  @Expose
  private List<RouteResponse> routes = null;

  public List<RouteResponse> getRoutes() {
    return routes;
  }

  public void setRoutes(List<RouteResponse> routes) {
    this.routes = routes;
  }

}
