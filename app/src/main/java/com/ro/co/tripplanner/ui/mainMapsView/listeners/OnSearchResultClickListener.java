package com.ro.co.tripplanner.ui.mainMapsView.listeners;


import androidx.annotation.NonNull;

public interface OnSearchResultClickListener {

  void OnSearchResultClicked(@NonNull Integer checkpointId);
}
