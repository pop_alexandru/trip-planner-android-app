package com.ro.co.tripplanner.ui.utils;


import androidx.annotation.NonNull;

public final class MapConstant {

    @NonNull
    static String MAP_URI_PREFIX = "https://www.google.com/maps/dir/?api=1";
    @NonNull
    static String MAP_URI_DESTINATION_PREFIX = "&destination=";
    @NonNull
    static String MAP_URI_WAYPOINTS_PREFX = "&waypoints=";

    private MapConstant() {
    }


}
