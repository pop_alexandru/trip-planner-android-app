package com.ro.co.tripplanner.domain.useCases;

import android.util.Pair;

import androidx.annotation.NonNull;

import com.ro.co.tripplanner.data.database.LocalStorageManager;
import com.ro.co.tripplanner.data.database.models.Checkpoint;
import com.ro.co.tripplanner.data.database.models.RouteLeg;
import com.ro.co.tripplanner.data.database.models.RouteStep;
import com.ro.co.tripplanner.domain.base.BaseUseCase;
import com.ro.co.tripplanner.domain.models.SelectedTripMapData;
import com.ro.co.tripplanner.ui.mainMapsView.models.MapCheckpoint;
import com.ro.co.tripplanner.ui.utils.MapUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import io.reactivex.Maybe;
import io.reactivex.MaybeSource;
import io.reactivex.Scheduler;
import io.reactivex.functions.Function;
import io.reactivex.functions.Function5;

public class LoadTripMapDataInBetweenCheckpointsUseCase extends BaseUseCase {

  @NonNull
  private LocalStorageManager storageManager;
  private int startCheckpoint;
  private int endCheckpoint;

  public LoadTripMapDataInBetweenCheckpointsUseCase(@NonNull Scheduler executorThread,
                                                    @NonNull Scheduler postExecutionThread,
                                                    @NonNull LocalStorageManager storageManager,
                                                    int startCheckpoint, int endCheckpoint) {
    super(executorThread, postExecutionThread);
    this.storageManager = storageManager;
    this.startCheckpoint = startCheckpoint;
    this.endCheckpoint = endCheckpoint;
  }

  @Override
  public Maybe<SelectedTripMapData> perform() {
    Maybe<List<MapCheckpoint>> mapCheckpoints = getMapCheckpointsBetween(startCheckpoint, endCheckpoint);
    Maybe<List<Pair<RouteLeg, List<RouteStep>>>> routeLegsAndSteps =
        getTripLegsAndSteps(startCheckpoint, endCheckpoint);
    Maybe<Integer> routeDistance = getRouteDistanceBetween(startCheckpoint, endCheckpoint);
    Maybe<Integer> routeDrivingTime = getRouteDrivingTimeBetween(startCheckpoint, endCheckpoint);
    Maybe<String> routeName = getTripName();

    return Maybe.zip(mapCheckpoints, routeLegsAndSteps, routeDistance, routeDrivingTime, routeName,
        mapTripMapData())
        .subscribeOn(executorThread)
        .observeOn(postExecutionThread);
  }

  private Function5<List<MapCheckpoint>, List<Pair<RouteLeg, List<RouteStep>>>, Integer,
      Integer, String, SelectedTripMapData> mapTripMapData() {
    return SelectedTripMapData::new;
  }

  private Maybe<Integer> getRouteDistanceBetween(int startCheckpointId,
                                                 int endCheckpointId) {
    return storageManager.checkpointsDao()
        .getDistanceBetweenTwoCheckpoints(startCheckpointId, endCheckpointId);
  }

  private Maybe<Integer> getRouteDrivingTimeBetween(int startCheckpointId,
                                                    int endCheckpointId) {
    return storageManager.checkpointsDao()
        .getDrivingTimeBetweenTwoCheckpoints(startCheckpointId, endCheckpointId);
  }

  private Maybe<String> getTripName() {
    return storageManager.tourDao()
        .getCurrentlySelectedTourName();
  }

  private Maybe<List<MapCheckpoint>> getMapCheckpointsBetween(int startCheckpointId,
                                                              int endCheckpointId) {
    return storageManager.checkpointsDao()
        .getAllCheckpointsBetweenStartAndEndCheckpointIds(startCheckpointId, endCheckpointId)
        .flatMap(mapRetrievedCheckpoints());
  }

  private Function<List<Checkpoint>, MaybeSource<List<MapCheckpoint>>> mapRetrievedCheckpoints() {
    return checkpoints -> Maybe.fromCallable(
        () -> MapUtils.convertToMapCheckpoints(checkpoints));
  }

  private Maybe<List<Pair<RouteLeg, List<RouteStep>>>> getTripLegsAndSteps(int startCheckpointId,
                                                                           int endCheckpointId) {
    // Substract -1 from endCheckpoint so you won't receive the next routeLeg and steps after the final id
    return storageManager.routeLegDao()
        .getRouteLegsForStartAndEndCheckpoints(startCheckpointId, endCheckpointId - 1)
        .flatMap(mapRoutesAndLegs());
  }

  private Function<List<RouteLeg>, MaybeSource<List<Pair<RouteLeg, List<RouteStep>>>>> mapRoutesAndLegs() {
    return routeLegs -> Maybe.fromCallable(new Callable<List<Pair<RouteLeg, List<RouteStep>>>>() {
      @Override
      public List<Pair<RouteLeg, List<RouteStep>>> call() {
        List<Pair<RouteLeg, List<RouteStep>>> routeLegsAndSteps = new ArrayList<>();

        for (RouteLeg routeLeg : routeLegs) {
          List<RouteStep> routeSteps = storageManager.routeStepDao()
              .getStepsForLegId(routeLeg.getRouteLegId());
          routeLegsAndSteps.add(new Pair<>(routeLeg, routeSteps));
        }
        return routeLegsAndSteps;
      }
    });
  }
}
