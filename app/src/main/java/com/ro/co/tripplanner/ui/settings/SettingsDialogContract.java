package com.ro.co.tripplanner.ui.settings;

import com.ro.co.tripplanner.ui.base.BaseContract;

public class SettingsDialogContract {

    public interface View extends BaseContract.View {

        void dismissDialog();

        void switchToLoginScreen();
    }

    public interface Presenter {

        void onDismissButtonClicked();

        void onSignOutButtonClicked();

    }

}
