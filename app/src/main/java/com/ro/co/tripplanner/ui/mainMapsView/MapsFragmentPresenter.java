package com.ro.co.tripplanner.ui.mainMapsView;

import android.Manifest;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.IBinder;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.room.EmptyResultSetException;

import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.data.LineData;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.maps.android.PolyUtil;
import com.ro.co.tripplanner.R;
import com.ro.co.tripplanner.app.Injection;
import com.ro.co.tripplanner.data.SharedPreferencesKeys;
import com.ro.co.tripplanner.data.database.models.Checkpoint;
import com.ro.co.tripplanner.data.database.models.RouteLeg;
import com.ro.co.tripplanner.data.database.models.RouteStep;
import com.ro.co.tripplanner.data.location.GpsLocationManager;
import com.ro.co.tripplanner.data.network.NetworkExceptions;
import com.ro.co.tripplanner.data.network.models.response.dailyTour.TripDetailsResponse;
import com.ro.co.tripplanner.domain.models.NavigationPathData;
import com.ro.co.tripplanner.domain.models.SelectedTripMapData;
import com.ro.co.tripplanner.domain.services.LocationsUpdatesService;
import com.ro.co.tripplanner.domain.services.RequestDirectionsService;
import com.ro.co.tripplanner.domain.useCases.LoadNextCheckpointsFromOriginPointUseCase;
import com.ro.co.tripplanner.domain.useCases.LoadTripMapDataInBetweenCheckpointsUseCase;
import com.ro.co.tripplanner.domain.useCases.LoadTripMapDataUseCase;
import com.ro.co.tripplanner.domain.useCases.QueryForCheckpointsUseCase;
import com.ro.co.tripplanner.domain.useCases.SaveTripDataLocallyUseCase;
import com.ro.co.tripplanner.ui.base.BasePresenter;
import com.ro.co.tripplanner.ui.mainMapsView.chartView.ChartDataCreatedListener;
import com.ro.co.tripplanner.ui.mainMapsView.chartView.ChartViewDataHandler;
import com.ro.co.tripplanner.ui.mainMapsView.chartView.ElevationPoint;
import com.ro.co.tripplanner.ui.mainMapsView.models.MapCheckpoint;
import com.ro.co.tripplanner.ui.mainMapsView.models.SearchResultModel;
import com.ro.co.tripplanner.ui.notifications.NotificationManager;
import com.ro.co.tripplanner.ui.utils.MapUtils;
import com.ro.co.tripplanner.ui.utils.NetworkUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Maybe;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class MapsFragmentPresenter extends BasePresenter
    implements MapsFragmentContract.Presenter, ServiceConnection, OnSuccessListener<Location>, ChartDataCreatedListener {

  @NonNull
  private final String TAG = MapsFragmentPresenter.class.getSimpleName();
  private final int NAVIGATION_CHECKPOINTS_SIZE = 11;

  @NonNull
  private final ServiceConnection serviceConnection = this;
  @NonNull
  private final MapsFragmentContract.View view;
  @NonNull
  private final List<LatLng> currentSelectedRoutePoints = new ArrayList<>();
  @NonNull
  private final List<Checkpoint> navigationPathWayPoints = new ArrayList<>();
  @NonNull
  private final ArrayList<MapCheckpoint> displayedTripCheckpoints = new ArrayList<>();
  @Nullable
  private Service locationUpdatesService;
  private boolean isServiceBound;

  MapsFragmentPresenter(@NonNull MapsFragmentContract.View view) {
    this.view = view;
  }

  @Override
  public void onMapReady() {
    if (isViewAttached) {
      loadEntireTourDataOnMap();
      if (Injection.provideSharedPreferences()
          .getBoolean(SharedPreferencesKeys.KEY_LOCATION_TRACKING_ENABLED, false)) {
        startLocationTracking();
      }
    }
  }

  @Override
  public void onDestroyView() {
    super.onDestroyView();
    displayedTripCheckpoints.clear();
    if (ActivityCompat.checkSelfPermission(Injection.provideGlobalContext(), Manifest.permission.ACCESS_FINE_LOCATION)
        == PackageManager.PERMISSION_GRANTED) {
      GpsLocationManager.getInstance()
          .stopRequestingLocationUpdates();
    }
  }

  @Override
  public void onUnBindDirectionsRequestService() {
    if (isServiceBound) {
      Injection.provideGlobalContext()
          .unbindService(serviceConnection);
      isServiceBound = false;
    }
  }

  @Override
  public void onCalculatingRoutesStarted() {
    if (isViewAttached) {
      view.showCalculatingDirectionsAnimation(true);
    }
  }

  @Override
  public void onCalculatingRoutesDone() {
    if (isViewAttached) {
      view.showCalculatingDirectionsAnimation(false);
    }
    loadEntireTourDataOnMap();
  }

  @Override
  public void onRoutesRequestsError(@NonNull String errorType) {
    if (isViewAttached) {
      if (TextUtils.equals(errorType, NetworkExceptions.UNKNOWN_HOST.name())) {
        view.showMessage(getString(R.string.error_message_no_internet_connection));
      } else if (TextUtils.equals(errorType, NetworkExceptions.STREAM_RESET_EXCEPTION.name())) {
        view.showMessage(getString(R.string.error_message_internet_connection_intrerupted));
      } else if (TextUtils.equals(errorType, NetworkExceptions.
          TRIP_DIRECTIONS_COULD_NOT_BE_OBTAINED_ERROR.name())) {
        view.showMessage(getString(R.string.error_message_directions_calculation_error));
      } else {
        view.showMessage(getString(R.string.error_message_directions_calculation_error));
      }
    }
    dismissLoadingView();
  }

  @Override
  public void onCurrentLocationChanged(@NonNull Location location) {
    if (isViewAttached) {
      LatLng locationLatLng = new LatLng(location.getLatitude(), location.getLongitude());
      view.updateCurrentUserLocation(locationLatLng);
      boolean areDeviationNotificationsEnabled = Injection.provideSharedPreferences()
          .getBoolean(SharedPreferencesKeys.KEY_ROUTE_DEVIATION_NOTIFICATIONS_ENABLED, false);
      if (currentSelectedRoutePoints.size() > 0 && areDeviationNotificationsEnabled) {
        boolean isLocationOnRoute = PolyUtil.isLocationOnPath(locationLatLng, currentSelectedRoutePoints, true, 1000);
        if (!isLocationOnRoute) {
          NotificationManager.getInstance()
              .notifyAboutRouteDeviation();
        }
      }
    }
  }

  @Override
  public void onNavigationClicked() {
    if (navigationPathWayPoints.size() != 0) {
      String navUri = MapUtils.composeUriForMapsIntentRequest(navigationPathWayPoints);
      if (isViewAttached) {
        view.startGoogleMapsDirections(navUri);
      }
    }
  }

  @Override
  public void onChooseTourClicked() {
    boolean isNetworkAvailable = NetworkUtils.isInternetConnectionAvailable(Injection.provideGlobalContext());
    if (isNetworkAvailable && isViewAttached) {
      view.showTourPickerDialog();
    } else {
      view.showMessage(getString(R.string.message_no_internet_reloading_saved_tour));
      loadEntireTourDataOnMap();
    }
  }

  @Override
  public void onTourSelected(@NonNull String tourId,
                             @NonNull List<TripDetailsResponse> tripDetailsResponses) {
    addSubscription(
        new SaveTripDataLocallyUseCase(Schedulers.io(), AndroidSchedulers.mainThread(),
            Injection.provideStorageManager(), tripDetailsResponses, tourId)
            .perform()
            .subscribe(this::startRouteDirectionsRequests));
  }

  @Override
  public void onSettingsClicked() {
    if (isViewAttached) {
      view.showSettingsDialog();
    }
  }

  @Override
  public void onFilterButtonClicked() {
    if (isViewAttached) {
      view.showFilteringOptionsView();
    }
  }

  @Override
  public void onServiceConnected(ComponentName name, IBinder service) {
    if (name.getClassName()
        .equals(LocationsUpdatesService.class.getName())) {
      LocationsUpdatesService.LocationServiceBinder binder = (LocationsUpdatesService.LocationServiceBinder) service;
      locationUpdatesService = binder.getService();
    }
  }

  @Override
  public void onServiceDisconnected(ComponentName name) {
    if (name.getClassName()
        .equals(LocationsUpdatesService.class.getName())) {
      locationUpdatesService = null;
    }
  }

  @Override
  public void OnSearchResultClicked(@NonNull Integer checkpointId) {
    if (isViewAttached) {
      view.hideSoftKeyboard();
      for (MapCheckpoint checkpoint : displayedTripCheckpoints) {
        Integer checkpointIdFromDisplayedPoint = checkpoint.getMapCheckpointId();
        if (checkpointId.equals(checkpointIdFromDisplayedPoint)) {
          view.moveCameraToCurrentLocation(checkpoint.getPosition());
        }
      }
      view.clearSearchResults();
    }
  }

  @Override
  public void onLocationTrackingSettingsUpdate(boolean isLocationTrackingEnabled) {
    if (isLocationTrackingEnabled) {
      startLocationTracking();
    } else {
      stopLocationTracking();
    }
  }

  @Override
  public void onSelectedCheckpointRouteFilters(@NonNull List<MapCheckpoint> toFilterRouteByCheckpoints) {
    int startCheckpointId = toFilterRouteByCheckpoints.get(0)
        .getMapCheckpointId();
    int endCheckpointId = toFilterRouteByCheckpoints.get(1)
        .getMapCheckpointId();

    if (startCheckpointId < endCheckpointId) {
      loadTripDataBetweenTwoCheckpointsOfARoute(startCheckpointId, endCheckpointId);
    } else {
      // If a user selects 2 checkpoints in reverse , switch then for the loading logic / database query to work
      loadTripDataBetweenTwoCheckpointsOfARoute(endCheckpointId, startCheckpointId);
    }
  }

  @Override
  public void onClearFilteredRouteClicked() {
    if (isViewAttached) {
      view.clearFilteringChipsSelectionStatus();
      loadEntireTourDataOnMap();
    }
  }

  @Override
  public void onFilterChipSelectionRemoved() {
    loadEntireTourDataOnMap();
  }

  @Override
  public void onMyLocationButtonClicked() {
    if (ActivityCompat.checkSelfPermission(Injection.provideGlobalContext(),
        Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
      GpsLocationManager.getInstance()
          .getLastKnownLocation(this);
    }
  }

  @Override
  public void onMarkerClicked(int markerCheckpointId) {
    List<MapCheckpoint> mapCheckpoints = displayedTripCheckpoints;
    int startCheckpointId = mapCheckpoints.get(0)
        .getMapCheckpointId();
    int endCheckpointId = mapCheckpoints.get(mapCheckpoints.size() - 1)
        .getMapCheckpointId();

    Single<NavigationPathData> observable =
        new LoadNextCheckpointsFromOriginPointUseCase(Schedulers.io(), AndroidSchedulers.mainThread(),
            Injection.provideStorageManager(), markerCheckpointId, NAVIGATION_CHECKPOINTS_SIZE,
            startCheckpointId, endCheckpointId)
            .perform();

    Disposable disposable = observable
        .filter(navigationPathData -> isViewAttached)
        .subscribe(this::onLoadNavigationPathSuccess, this::onLoadNavigationPathError);
    addSubscription(disposable);
  }

  private void onLoadNavigationPathSuccess(NavigationPathData navigationPathData) {
    if (navigationPathData != null) {
      view.highLightNavigationPath(navigationPathData.getNavigationPathRouteLegs());
      view.showSelectTripButton(false);
      view.showNavigationButton(true);
      navigationPathWayPoints.addAll(navigationPathData.getNavigationPathWayPoints());
    }
  }

  private void onLoadNavigationPathError(Throwable throwable) {
    Log.e(TAG, "Error loading navigation path data " + throwable.getMessage());
    view.showMessage(getString(R.string.error_message_generic_error));
  }

  @Override
  public void onMarkerInfoWindowClosed() {
    if (isViewAttached) {
      navigationPathWayPoints.clear();
      view.clearAllHighlightedPaths();
      view.showNavigationButton(false);
      view.showSelectTripButton(true);
    }
  }

  private void startRouteDirectionsRequests() {
    Context context = Injection.provideGlobalContext();
    Intent serviceIntent = new Intent(context, RequestDirectionsService.class);
    context.startService(serviceIntent);
    context.bindService(serviceIntent, serviceConnection, Context.BIND_AUTO_CREATE);
    isServiceBound = true;
  }

  private void startLocationTracking() {
    Context context = Injection.provideGlobalContext();
    Intent serviceIntent = new Intent(context, LocationsUpdatesService.class);
    context.startService(serviceIntent);
    context.bindService(serviceIntent, serviceConnection, Context.BIND_AUTO_CREATE);
    isServiceBound = true;
  }

  private void stopLocationTracking() {
    if (locationUpdatesService != null) {
      locationUpdatesService.stopSelf();
    }
  }

  /*
   * Loads tour data that is filtered between 2 checkpoints ids, showing only the route polyline and elevation data
   * between the 2
   */
  private void loadTripDataBetweenTwoCheckpointsOfARoute(int startCheckpointId, int endCheckpointId) {

    Maybe<SelectedTripMapData> getSelectedTripMapDataObservable =
        new LoadTripMapDataInBetweenCheckpointsUseCase(Schedulers.io(), AndroidSchedulers.mainThread(),
            Injection.provideStorageManager(), startCheckpointId, endCheckpointId).perform();

    addSubscription(getSelectedTripMapDataObservable
        .doOnSubscribe(subscription -> clearMapAndDisplayLoadingProgressBar())
        .subscribe(this::onGetTripMapDataSuccess, this::onGetMapDataError));
  }

  /*
   * Loads entire tour with associated checkpoints and polyline extracted from the RouteSteps and Legs that form the route
   */
  private void loadEntireTourDataOnMap() {
    Maybe<SelectedTripMapData> getSelectedTripMapDataObservable =
        new LoadTripMapDataUseCase(Schedulers.io(), AndroidSchedulers.mainThread(),
            Injection.provideStorageManager()).perform();

    addSubscription(getSelectedTripMapDataObservable
        .doOnSubscribe(subscription -> clearMapAndDisplayLoadingProgressBar())
        .subscribe(this::onGetTripMapDataSuccess, this::onGetMapDataError));
  }

  private void onGetMapDataError(@NonNull Throwable throwable) {
    Log.e(TAG, "Error trying to get the map data from database " + throwable.getMessage());
    dismissLoadingView();
    if (throwable instanceof EmptyResultSetException) {
      displayNoRouteSelectedWarning();
    } else {
      view.showMessage(getString(R.string.error_message_generic_error));
    }
  }

  private void onGetTripMapDataSuccess(SelectedTripMapData selectedTripMapData) {
    dismissLoadingView();

    List<MapCheckpoint> mapCheckpoints = selectedTripMapData.getTripCheckpoints();
    if (mapCheckpoints != null) {
      loadMapCheckpointsAndFilteringOptionsOnMapView(mapCheckpoints);
      loadElevationChartData(mapCheckpoints);
    }

    List<Pair<RouteLeg, List<RouteStep>>> tripPolylineData =
        selectedTripMapData.getTripPolylineData();
    if (tripPolylineData != null) {
      loadRoutePolylineOnMapView(tripPolylineData);
    }

    String routeTitle = selectedTripMapData.getTripTitle();
    Integer routeDistance = selectedTripMapData.getTripDistance();
    Integer routeDuration = selectedTripMapData.getTripDuration();
    if (routeTitle != null && routeDistance != null && routeDuration != null) {
      Pair<String, String> formattedRouteInfo = MapUtils.generateInfoMessage(new Pair<>(routeDistance, routeDuration));
      view.showTotalRouteInformation(formattedRouteInfo.first, formattedRouteInfo.second, routeTitle);
    }
  }

  private void displayNoRouteSelectedWarning() {
    String noInfoAvailable = getString(R.string.message_no_information_available);
    view.showTotalRouteInformation(noInfoAvailable, noInfoAvailable, getString(R.string.title_no_tour_selected));
    view.animateRouteSelectionButton();
    view.animateRouteInformationText();
  }

  private void clearMapAndDisplayLoadingProgressBar() {
    if (isViewAttached) {
      view.showLoadingView(true);
      // Clear stored map checkpoints in the Presenter object
      displayedTripCheckpoints.clear();
      // Clear markers and routes from map
      view.clearMapCheckpoints();
      view.clearMapRoutes();
    }
  }

  private void loadMapCheckpointsAndFilteringOptionsOnMapView(@NonNull List<MapCheckpoint> mapCheckpoints) {
    if (mapCheckpoints.size() > 0) {
      // Clear previous map points
      displayedTripCheckpoints.clear();
      displayedTripCheckpoints.addAll(mapCheckpoints);
      // Load new trip checkpoints and center to route
      view.loadCheckpointsOnMapView(mapCheckpoints);
      view.centerMapToCurrentSelectedRoute(mapCheckpoints);
      view.loadAvailableFilterPoints(mapCheckpoints);
    }
  }

  private void loadRoutePolylineOnMapView(@NonNull Iterable<Pair<RouteLeg, List<RouteStep>>> routeLegsStepsList) {
    for (Pair<RouteLeg, List<RouteStep>> routeLegStepsPair : routeLegsStepsList) {
      List<RouteStep> steps = routeLegStepsPair.second;
      int routeLegId = routeLegStepsPair.first.getRouteLegId();
      List<LatLng> routeLegPolylinePoints = new ArrayList<>();
      for (RouteStep routeStep : steps) {
        List<LatLng> stepLinePoints = MapUtils.convertPolyLineToMapPoints(routeStep.getRouteStepPolyline());
        routeLegPolylinePoints.addAll(stepLinePoints);
      }
      drawRouteStepFromMapPoints(routeLegPolylinePoints, routeLegId);
      currentSelectedRoutePoints.addAll(routeLegPolylinePoints);
    }
  }

  private void dismissLoadingView() {
    if (isViewAttached) {
      view.showLoadingView(false);
    }
  }

  private void loadElevationChartData(@NonNull List<MapCheckpoint> mapCheckpoints) {
    Observable<List<ElevationPoint>> loadElevationDataObservable =
        Observable.fromArray(mapCheckpoints)
            .flatMap(mapElevationPointData())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .filter(elevationPointList -> isViewAttached);

    addSubscription(loadElevationDataObservable
        .subscribe(this::onLoadAltitudeChartData));
  }

  private Function<List<MapCheckpoint>, ObservableSource<List<ElevationPoint>>> mapElevationPointData() {
    return mapCheckpoints -> {
      List<ElevationPoint> elevationPoints = new ArrayList<>();
      for (MapCheckpoint mapCheckpoint : mapCheckpoints) {
        if (mapCheckpoint.getAltitude() != null) {
          float altitude = BigDecimal.valueOf(mapCheckpoint.getAltitude()).floatValue();
          ElevationPoint point = new ElevationPoint(mapCheckpoint.getOrderInRouteId(),
              altitude);
          elevationPoints.add(point);
        }
      }
      return Observable.just(elevationPoints);
    };
  }

  private void onLoadAltitudeChartData(@NonNull List<ElevationPoint> elevationPoints) {
    if (elevationPoints.size() > 0) {
      new ChartViewDataHandler(elevationPoints, this);
    }
  }

  private void drawRouteStepFromMapPoints(@NonNull Iterable<LatLng> routeMapPoints, int routeStepId) {
    PolylineOptions routePolyline = MapUtils.generateRoute(routeMapPoints);
    if (isViewAttached) {
      view.drawRouteStepLineOnMap(routePolyline, routeStepId);
    }
  }

  @Override
  public void onSuccess(Location location) {
    if (location != null) {
      LatLng currentLocation = new LatLng(location.getLatitude(), location.getLongitude());
      view.moveCameraToCurrentLocation(currentLocation);
    }
  }

  @Override
  public void OnChartDataCreated(@NonNull LineData lineData, @NonNull Description description) {
    if (isViewAttached) {
      view.showChartView(lineData, description);
    }
  }

  @Override
  public boolean onQueryTextChange(String searchedQuery) {
    if (searchedQuery.equals("")) {
      if (isViewAttached) {
        view.clearSearchResults();
      }
    } else {
      addSubscription(new QueryForCheckpointsUseCase(Schedulers.io(), AndroidSchedulers.mainThread(),
          displayedTripCheckpoints, searchedQuery.toLowerCase())
          .perform()
          .filter(mapCheckpoints -> isViewAttached)
          .subscribe(this::onQueryForCheckpointsSuccess, this::onQueryForCheckpointsFailed));
    }
    return false;
  }

  private void onQueryForCheckpointsSuccess(List<MapCheckpoint> checkpoints) {
    List<SearchResultModel> searchResultModels = new ArrayList<>();
    for (MapCheckpoint details : checkpoints) {
      SearchResultModel searchResultModel = new SearchResultModel(details.getMapCheckpointId(),
          String.valueOf(details.getOrderInRouteId()), details.getMapCheckpointTitle(), this);
      searchResultModels.add(searchResultModel);
    }
    view.displaySearchResults(searchResultModels);
  }

  private void onQueryForCheckpointsFailed(@NonNull Throwable throwable) {
    Log.e(TAG, "Failed to query checkpoints " + throwable.getMessage());
    view.showMessage(getString(R.string.error_message_generic_error));
  }

  @Override
  public boolean onSearchViewClosed(boolean isClosed) {
    if (isViewAttached && isClosed) {
      view.searchViewClosed();
    }
    return false;
  }

  @Override
  public void onClick(View v) {
    switch (v.getId()) {
      case R.id.searchViewCheckpoints:
        if (isViewAttached) {
          view.searchViewOpen();
        }
        break;
    }
  }
}
