package com.ro.co.tripplanner.data.network.models.response.dailyTour;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TripCheckpoint {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("tripOrderId")
    @Expose
    private Integer tripOrderId;
  @SerializedName("name")
    @Expose
  private String name;
    @SerializedName("latitude")
    @Expose
    private Double latitude;
    @SerializedName("longitude")
    @Expose
    private Double longitude;
  @SerializedName("altitude")
  private Double altitude;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

  public Integer getOrderInTrip() {
        return tripOrderId;
    }

  public String getName() {
    return name;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

  public Double getAltitude() {
    return altitude;
  }
}
