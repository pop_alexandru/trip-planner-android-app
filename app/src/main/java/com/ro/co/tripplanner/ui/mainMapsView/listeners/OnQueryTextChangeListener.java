package com.ro.co.tripplanner.ui.mainMapsView.listeners;

public interface OnQueryTextChangeListener {

  boolean onQueryTextChange(String newText);
}
