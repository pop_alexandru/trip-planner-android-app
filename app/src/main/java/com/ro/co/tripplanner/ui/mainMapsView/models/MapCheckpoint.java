package com.ro.co.tripplanner.ui.mainMapsView.models;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;


public class MapCheckpoint implements ClusterItem {

  @NonNull
  private final Integer mapCheckpointId;
  @NonNull
  private final LatLng position;
  @Nullable
  private final Double altitude;
  @NonNull
  private final Integer orderInRouteId;
  @NonNull
  private final String mapCheckpointTitle;
  @NonNull
  private final String mapCheckpointDetails;
  private final int markerIconColor;

  public MapCheckpoint(@NonNull Integer mapCheckpointId,
                       @NonNull LatLng position,
                       @Nullable Double altitude,
                       @NonNull Integer orderInRouteId,
                       @NonNull String mapCheckpointTitle,
                       @NonNull String mapCheckpointDetails,
                       int markerIconColor) {
    this.mapCheckpointId = mapCheckpointId;
    this.position = position;
    this.altitude = altitude;
    this.orderInRouteId = orderInRouteId;
    this.mapCheckpointTitle = mapCheckpointTitle;
    this.mapCheckpointDetails = mapCheckpointDetails;
    this.markerIconColor = markerIconColor;
  }

  @NonNull
  @Override
  public LatLng getPosition() {
    return position;
  }

  @Override
  public String getTitle() {
    return null;
  }

  @Override
  public String getSnippet() {
    return null;
  }

  @NonNull
  public Integer getMapCheckpointId() {
    return mapCheckpointId;
  }

  @NonNull
  public String getMapCheckpointTitle() {
    return mapCheckpointTitle;
  }

  @NonNull
  public String getMapCheckpointDetails() {
    return mapCheckpointDetails;
  }

  @NonNull
  public int getMarkerIconColor() {
    return markerIconColor;
  }

  @NonNull
  public Integer getOrderInRouteId() {
    return orderInRouteId;
  }

  @Nullable
  public Double getAltitude() {
    return altitude;
  }
}
