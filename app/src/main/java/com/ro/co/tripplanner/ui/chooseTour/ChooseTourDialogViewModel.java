package com.ro.co.tripplanner.ui.chooseTour;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableList;

import com.ro.co.tripplanner.BR;
import com.ro.co.tripplanner.R;

import me.tatarka.bindingcollectionadapter2.ItemBinding;

public class ChooseTourDialogViewModel {

  @NonNull
  public ItemBinding<TourModel> itemBinding = ItemBinding.of(BR.viewModel, R.layout.item_route_selection);
  @NonNull
  public ObservableList<TourModel> availableTours = new ObservableArrayList<>();
  @NonNull
  public ObservableBoolean isLoadingInProgress = new ObservableBoolean();

}
