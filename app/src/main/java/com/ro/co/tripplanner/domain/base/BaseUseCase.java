package com.ro.co.tripplanner.domain.base;


import androidx.annotation.NonNull;

import io.reactivex.Scheduler;


public abstract class BaseUseCase implements UseCaseDefinition {

  @NonNull
  protected final Scheduler executorThread;
  @NonNull
  protected final Scheduler postExecutionThread;

  public BaseUseCase(@NonNull Scheduler executorThread, @NonNull Scheduler postExecutionThread) {
    this.executorThread = executorThread;
    this.postExecutionThread = postExecutionThread;
  }
}
