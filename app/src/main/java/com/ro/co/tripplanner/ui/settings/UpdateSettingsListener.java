package com.ro.co.tripplanner.ui.settings;

public interface UpdateSettingsListener {

    void OnLocationTrackingSettingsUpdate(boolean isLocationTrackingEnabled);
}
