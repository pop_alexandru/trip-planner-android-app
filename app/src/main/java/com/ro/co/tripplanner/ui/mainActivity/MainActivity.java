package com.ro.co.tripplanner.ui.mainActivity;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.ro.co.tripplanner.R;
import com.ro.co.tripplanner.ui.mainMapsView.MapsFragmentView;

public class MainActivity extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    DataBindingUtil.setContentView(this, R.layout.activity_main);
    setupViewTripsMapFragment();
  }

  private void setupViewTripsMapFragment() {
    MapsFragmentView fragmentView = MapsFragmentView.createInstance();
    replaceInFragmentManager(fragmentView, MapsFragmentView.TAG);
  }

  private void replaceInFragmentManager(@NonNull Fragment fragment, @NonNull String tag) {
    getSupportFragmentManager().beginTransaction()
        .replace(R.id.main_content_frame, fragment, tag)
        .commit();
  }
}
