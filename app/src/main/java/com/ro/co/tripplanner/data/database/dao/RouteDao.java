package com.ro.co.tripplanner.data.database.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.ro.co.tripplanner.data.database.models.Route;

import java.util.List;

@Dao
public interface RouteDao {

  @Insert(onConflict = OnConflictStrategy.REPLACE)
  long insert(Route route);

  @Query("DELETE FROM ROUTE")
  int deleteAll();

  @Query("Select * from ROUTE WHERE tourId = :tourId")
  List<Route> getRoutesForTourId(String tourId);
}
