package com.ro.co.tripplanner.data.network.models.response.routes;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Polyline {

  @SerializedName("points")
  @Expose
  private String points;

  public String getPoints() {
    return points;
  }

}
