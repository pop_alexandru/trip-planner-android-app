package com.ro.co.tripplanner.data.network;

import androidx.annotation.NonNull;

import com.google.android.gms.maps.model.LatLng;
import com.ro.co.tripplanner.data.database.models.Checkpoint;
import com.ro.co.tripplanner.data.network.models.request.RouteDirectionsRequest;

import java.util.ArrayList;
import java.util.List;

public final class NetworkRequestBuilders {

  @NonNull
  private static final String DIRECTIONS_REQUEST_MODE = "driving";

  private NetworkRequestBuilders() {
  }

  @NonNull
  public static RouteDirectionsRequest generateDirectionRequestParameters(@NonNull List<Checkpoint> checkpoints, @NonNull String apiKey) {
    List<LatLng> checkpointLatLng = new ArrayList<>();
    for (int index = 0; index < checkpoints.size(); index++) {
      Checkpoint checkpoint = checkpoints.get(index);
      checkpointLatLng.add(new LatLng(checkpoint.getLatitude(), checkpoint.getLongitude()));
    }

    LatLng startCheckpoint = checkpointLatLng.get(0);
    LatLng endCheckpoint = checkpointLatLng.get(checkpointLatLng.size() - 1);
    checkpointLatLng.remove(startCheckpoint);
    checkpointLatLng.remove(endCheckpoint);
    return new RouteDirectionsRequest.RouteParametersBuilder().setStartWaypoint(startCheckpoint)
        .setEndWaypoint(endCheckpoint)
        .setTransitWaypoints(checkpointLatLng)
        .setMode(NetworkRequestBuilders.DIRECTIONS_REQUEST_MODE)
        .setAPIKey(apiKey)
        .createRouteParameters();
  }
}
