package com.ro.co.tripplanner.ui.chooseTour;

public interface TourClickedListener {

    void OnTourClicked(String tourId);
}
