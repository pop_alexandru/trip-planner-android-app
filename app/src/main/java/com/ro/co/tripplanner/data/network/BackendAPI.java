package com.ro.co.tripplanner.data.network;

import com.ro.co.tripplanner.data.network.models.response.dailyTour.TripDetailsResponse;
import com.ro.co.tripplanner.data.network.models.response.dailyTour.TripResponse;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface BackendAPI {

  @GET("trips/get-trips-list/")
  Observable<List<TripResponse>> getAllTours();

  @GET("trips/get-trip-details/{id}")
  Observable<TripDetailsResponse> getTourById(@Path("id") String tourId);

}
