package com.ro.co.tripplanner.ui.mainMapsView.listeners;

import androidx.annotation.NonNull;

import com.ro.co.tripplanner.data.network.models.response.dailyTour.TripDetailsResponse;

import java.util.List;

public interface OnSelectedTourListener {

  void OnSelectedTour(@NonNull String tourId, List<TripDetailsResponse> tripDetailsRespons);
}
