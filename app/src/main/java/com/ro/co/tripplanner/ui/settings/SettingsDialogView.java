package com.ro.co.tripplanner.ui.settings;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;

import com.ro.co.tripplanner.BuildConfig;
import com.ro.co.tripplanner.R;
import com.ro.co.tripplanner.app.Injection;
import com.ro.co.tripplanner.data.SharedPreferencesKeys;
import com.ro.co.tripplanner.databinding.FragmentDialogSettingsBinding;
import com.ro.co.tripplanner.ui.base.BaseDialogFragment;

public class SettingsDialogView extends BaseDialogFragment<SettingsDialogPresenter>
    implements SettingsDialogContract.View, CompoundButton.OnCheckedChangeListener {

    @NonNull
    public static final String TAG = SettingsDialogView.class.getSimpleName();
    @NonNull
    private final SharedPreferences preferences = Injection.provideSharedPreferences();
    @NonNull
    private final SettingsDialogViewModel viewModel = new SettingsDialogViewModel();
    @NonNull
    private FragmentDialogSettingsBinding viewBinding;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        viewBinding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.fragment_dialog_settings, null, false);
        viewBinding.setPresenter(getPresenter());
        viewBinding.setViewModel(viewModel);
        viewBinding.switchLocation.setOnCheckedChangeListener(this);
        viewBinding.switchDeviationNotifications.setOnCheckedChangeListener(this);
        setupCurrentSettings();
        setupTransparentDialogBackground();
        return viewBinding.getRoot();
    }

    private void setupCurrentSettings() {
        boolean areLocationUpdatesEnabled = preferences.getBoolean(SharedPreferencesKeys.KEY_LOCATION_TRACKING_ENABLED, false);
        boolean areDeviationNotificationsEnabled = preferences.getBoolean(SharedPreferencesKeys.KEY_ROUTE_DEVIATION_NOTIFICATIONS_ENABLED, false);
        if (areLocationUpdatesEnabled) {
            viewBinding.switchLocation.setChecked(true);
        }
        if (areDeviationNotificationsEnabled) {
            viewBinding.switchDeviationNotifications.setChecked(true);
        }
        String appVersion = BuildConfig.VERSION_NAME;
        String appVersionPrefix = getString(R.string.application_version);
        viewModel.appVersion.set(getString(R.string.format_app_version, appVersionPrefix, appVersion));
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        UpdateSettingsListener callback = (UpdateSettingsListener) getParentFragment();
        switch (buttonView.getId()) {
            case R.id.switchLocation:
                if (callback != null) {
                    updateLocationUpdateStatus(isChecked);
                    callback.OnLocationTrackingSettingsUpdate(isChecked);
                }
                break;
            case R.id.switchDeviationNotifications:
                updateRouteDeviationNotificationStatus(isChecked);
                break;
        }
    }

    private boolean updateLocationUpdateStatus(boolean areLocationTrackingEnabled) {
        return preferences.edit()
            .putBoolean(SharedPreferencesKeys.KEY_LOCATION_TRACKING_ENABLED, areLocationTrackingEnabled)
            .commit();

    }

    private boolean updateRouteDeviationNotificationStatus(boolean areRouteDeviationNotificationEnabled) {
        return preferences.edit()
            .putBoolean(SharedPreferencesKeys.KEY_ROUTE_DEVIATION_NOTIFICATIONS_ENABLED, areRouteDeviationNotificationEnabled)
            .commit();
    }

    @Override
    public void dismissDialog() {
        dismiss();
    }

    @Override
    public void switchToLoginScreen() {
        Activity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @Override
    public void showLoadingView(boolean isLoading) {
    }

    @Override
    public void showMessage(@NonNull String msg) {
    }

    @Override
    public SettingsDialogPresenter createPresenter() {
        return new SettingsDialogPresenter(this);
    }
}
