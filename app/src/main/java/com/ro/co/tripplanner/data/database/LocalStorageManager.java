package com.ro.co.tripplanner.data.database;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.ro.co.tripplanner.data.database.dao.CheckpointsDao;
import com.ro.co.tripplanner.data.database.dao.RouteDao;
import com.ro.co.tripplanner.data.database.dao.RouteLegDao;
import com.ro.co.tripplanner.data.database.dao.RouteStepDao;
import com.ro.co.tripplanner.data.database.dao.TourDao;
import com.ro.co.tripplanner.data.database.models.Checkpoint;
import com.ro.co.tripplanner.data.database.models.Route;
import com.ro.co.tripplanner.data.database.models.RouteLeg;
import com.ro.co.tripplanner.data.database.models.RouteStep;
import com.ro.co.tripplanner.data.database.models.Tour;


@Database(entities = {Checkpoint.class, Route.class, Tour.class, RouteStep.class, RouteLeg.class}, version = 6)
public abstract class LocalStorageManager extends RoomDatabase {

  @NonNull
  private static final String DATABASE_NAME = "grand_tour_database";
  private static LocalStorageManager instance;

  @NonNull
  public static LocalStorageManager getInstance(@NonNull Context context) {
    if (LocalStorageManager.instance == null) {
      LocalStorageManager.instance = Room.databaseBuilder(context.getApplicationContext(), LocalStorageManager.class, LocalStorageManager.DATABASE_NAME)
          .allowMainThreadQueries()
          .fallbackToDestructiveMigration()
          .build();
    }
    return LocalStorageManager.instance;
  }

  public abstract TourDao tourDao();

  public abstract CheckpointsDao checkpointsDao();

  public abstract RouteDao routeDao();

  public abstract RouteLegDao routeLegDao();

  public abstract RouteStepDao routeStepDao();
}
