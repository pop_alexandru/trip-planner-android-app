package com.ro.co.tripplanner.data.database.models;


import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import static androidx.room.ForeignKey.CASCADE;

@Entity(foreignKeys = @ForeignKey(entity = Tour.class, parentColumns = "tourId", childColumns = "tourId", onDelete = CASCADE))
public class Route {

  @PrimaryKey(autoGenerate = true)
  private int routeId;
  private String tourId;
  private String routePolyline;
  private int distance;

  public int getRouteId() {
    return routeId;
  }

  public void setRouteId(int routeId) {
    this.routeId = routeId;
  }

  public int getDistance() {
    return distance;
  }

  public void setDistance(int distance) {
    this.distance = distance;
  }

  public String getTourId() {
    return tourId;
  }

  public void setTourId(String tourId) {
    this.tourId = tourId;
  }

  public String getRoutePolyline() {
    return routePolyline;
  }

  public void setRoutePolyline(String routePolyline) {
    this.routePolyline = routePolyline;
  }
}
