package com.ro.co.tripplanner.domain.useCases;

import android.util.Pair;

import androidx.annotation.NonNull;

import com.ro.co.tripplanner.data.database.LocalStorageManager;
import com.ro.co.tripplanner.data.database.models.Checkpoint;
import com.ro.co.tripplanner.data.database.models.Route;
import com.ro.co.tripplanner.data.database.models.RouteLeg;
import com.ro.co.tripplanner.data.database.models.RouteStep;
import com.ro.co.tripplanner.domain.base.BaseUseCase;
import com.ro.co.tripplanner.domain.models.SelectedTripMapData;
import com.ro.co.tripplanner.ui.mainMapsView.models.MapCheckpoint;
import com.ro.co.tripplanner.ui.utils.MapUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import io.reactivex.Maybe;
import io.reactivex.MaybeSource;
import io.reactivex.Scheduler;
import io.reactivex.SingleSource;
import io.reactivex.functions.Function;
import io.reactivex.functions.Function5;

public class LoadTripMapDataUseCase extends BaseUseCase {

  @NonNull
  private final LocalStorageManager storageManager;

  public LoadTripMapDataUseCase(@NonNull Scheduler executorThread,
                                @NonNull Scheduler postExecutionThread,
                                @NonNull LocalStorageManager storageManager) {
    super(executorThread, postExecutionThread);
    this.storageManager = storageManager;
  }

  @Override
  public Maybe<SelectedTripMapData> perform() {
    Maybe<List<MapCheckpoint>> mapCheckpoints = getMapCheckpoints();
    Maybe<List<Pair<RouteLeg, List<RouteStep>>>> routeLegsAndSteps = getTripLegsAndSteps();
    Maybe<Integer> routeDistance = getRouteDistance();
    Maybe<Integer> routeDrivingTime = getRouteDrivingTime();
    Maybe<String> routeName = getSelectedRouteName();

    return Maybe.zip(mapCheckpoints, routeLegsAndSteps, routeDistance, routeDrivingTime, routeName,
        mapTripMapData())
        .subscribeOn(executorThread)
        .observeOn(postExecutionThread);
  }

  private Function5<List<MapCheckpoint>, List<Pair<RouteLeg, List<RouteStep>>>, Integer,
      Integer, String, SelectedTripMapData> mapTripMapData() {
    return SelectedTripMapData::new;
  }

  private Maybe<List<Pair<RouteLeg, List<RouteStep>>>> getTripLegsAndSteps() {
    return storageManager.tourDao()
        .getCurrentlySelectedTripId()
        .flatMapMaybe(mapRouteStepsAndLegsData());
  }

  private Maybe<Integer> getRouteDistance() {
    return storageManager.checkpointsDao()
        .getDistanceForEntireTour();
  }

  private Maybe<Integer> getRouteDrivingTime() {
    return storageManager.checkpointsDao()
        .getDrivingTimeForEntireTour();
  }

  private Maybe<String> getSelectedRouteName() {
    return storageManager.tourDao()
        .getCurrentlySelectedTourName();
  }

  private Maybe<List<MapCheckpoint>> getMapCheckpoints() {
    return storageManager.tourDao()
        .getCurrentlySelectedTripId()
        .flatMap(getRouteCheckpoints())
        .flatMapMaybe(mapRetrievedCheckpoints());
  }

  private Function<String, MaybeSource<List<Pair<RouteLeg, List<RouteStep>>>>> mapRouteStepsAndLegsData() {
    return tourId -> Maybe.fromCallable(new Callable<List<Pair<RouteLeg, List<RouteStep>>>>() {
      @Override
      public List<Pair<RouteLeg, List<RouteStep>>> call() {
        List<Route> routes = storageManager.routeDao()
            .getRoutesForTourId(tourId);

        List<Pair<RouteLeg, List<RouteStep>>> routeStepList = new ArrayList<>();

        for (Route route : routes) {
          int routeId = route.getRouteId();
          List<RouteLeg> routeLegs = storageManager.routeLegDao()
              .getRouteLegsForTourId(routeId);

          for (RouteLeg routeLeg : routeLegs) {
            int routeLegId = routeLeg.getRouteLegId();
            List<RouteStep> steps = storageManager.routeStepDao()
                .getStepsForLegId(routeLegId);
            routeStepList.add(new Pair<>(routeLeg, steps));
          }
        }
        return routeStepList;
      }
    });
  }

  private Function<String, SingleSource<List<Checkpoint>>> getRouteCheckpoints() {
    return tripId -> storageManager.checkpointsDao()
        .getAllCheckpointsForTripId(tripId);
  }

  private Function<List<Checkpoint>, MaybeSource<List<MapCheckpoint>>> mapRetrievedCheckpoints() {
    return checkpoints -> Maybe.fromCallable(
        () -> MapUtils.convertToMapCheckpoints(checkpoints));
  }
}
