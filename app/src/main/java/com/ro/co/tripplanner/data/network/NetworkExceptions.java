package com.ro.co.tripplanner.data.network;

public enum NetworkExceptions {
  UNKNOWN_HOST,
  STREAM_RESET_EXCEPTION,
  GENERIC_ERROR,
  TRIP_DIRECTIONS_COULD_NOT_BE_OBTAINED_ERROR
}
